//
//  mapsViewController.swift
//  jenosize
//
//  Created by Giftfy on 23/9/2563 BE.
//  Copyright © 2563 Giftfy. All rights reserved.
//

import UIKit
import GoogleMaps

class mapsViewController: UIViewController , GMSMapViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func loadView() {
        let camera = GMSCameraPosition.camera(withLatitude: 13.893983, longitude: 100.516292, zoom: 20)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: 13.893983, longitude: 100.516292)
        marker.title = "JENOSIZE"
        marker.map = mapView
        
    }
   
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
