//
//  API.swift
//  jenosize
//
//  Created by Giftfy on 23/9/2563 BE.
//  Copyright © 2563 Giftfy. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import ObjectMapper
fileprivate let apiUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
fileprivate let apiKey = "AIzaSyDLqXNWpVU4TuuGPpLlKASmLMU8adk8lKQ"
protocol FindRestaurantServiceProtocal {
    func fetchRestaurantFromGoogleAPI(lat: Double, long: Double,keyword : String, radius:      Int, type: String, completion: @escaping   ([Result?]) -> () )
}
class FindRestaurantService: FindRestaurantServiceProtocal {
    
    func fetchRestaurantFromGoogleAPI(lat: Double, long: Double,keyword : String, radius: Int, type: String, completion: @escaping ([Result?]) -> ()) {
         let parameters: Parameters = [
               "location" : "\(lat),\(long)",
               "radius": radius,
               "type" : type,
               "keyword" : keyword,
               "key": apiKey,
               ]
        var returnData : [Result] = []
        
        AF.request(apiUrl, method: .get , parameters: parameters).validate()
               .responseJSON { response in
               
                    if let data = response.value{
                        let jsonData = JSON(data)
                        print(jsonData)
                        
                        if let arrData = jsonData["results"].array {
                            for item in arrData {
                                print(item)
                                
                                let rowData: Result = Result(JSONString: item.rawString()!)!
                                   returnData.append(rowData)
                                }
                             completion(returnData)
                            
                            } else {
                              completion(returnData)
                            }
                        
                        
                    }
                    
               }
            
        }
}
        
    
    

