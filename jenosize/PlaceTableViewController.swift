//
//  PlaceTableViewController.swift
//  jenosize
//
//  Created by Giftfy on 23/9/2563 BE.
//  Copyright © 2563 Giftfy. All rights reserved.
//

import UIKit
import GooglePlaces


class PlaceTableViewController: UITableViewController, UISearchBarDelegate {
  
    @IBOutlet weak var SearchBar: UISearchBar!

    var findRestService: FindRestaurantService = FindRestaurantService()
    var dataRestLocation: [Result?] = []
    var geo : [Geometry?] = []
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.SearchBar.delegate = self
       

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.findRestService.fetchRestaurantFromGoogleAPI(lat: 13.893983, long: 100.516292,keyword: "",radius: 1500, type: "restaurant") { data in
                print(data.count)
                for item in data{
                
                    self.dataRestLocation.append(item)
                    self.geo.append(item?.geometry)
                }
                self.tableView.reloadData()
                }
       
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return dataRestLocation.count

    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : TableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell

        cell.lblTitle.text = dataRestLocation[indexPath.row]?.name
        cell.lblDesc.text = dataRestLocation[indexPath.row]?.vicinity
        if let url = URL( string:dataRestLocation[indexPath.row]!.icon)
        {
            DispatchQueue.global().async {
              if let data = try? Data( contentsOf:url)
              {
                DispatchQueue.main.async {
                  cell.icon.image = UIImage( data:data)
                }
              }
           }
        }


        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
   

            if (UIApplication.shared.canOpenURL(URL(string:"https://www.google.com/maps/")!)) {
                
                let strURL = "https://maps.google.com/?q=\(dataRestLocation[indexPath.row]?.name ?? "" )&center=\(geo[indexPath.row]?.lat ?? 0),\(geo[indexPath.row]?.lng ?? 0)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            print(strURL)
                let url = URL(string: strURL!)
                UIApplication.shared.open(url!) { (status) in
                        print(status)
                }
            } else {
                print("Can't use comgooglemaps://");
            }
       
        
      
    }
    
public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            print(searchText)
        dataRestLocation.removeAll()
        geo.removeAll()
        self.findRestService.fetchRestaurantFromGoogleAPI(lat: 13.893983, long: 100.516292,keyword: searchText,radius: 1500, type: "restaurant") { data in
                       print(data.count)
                       for item in data{
                       
                           self.dataRestLocation.append(item)
                           self.geo.append(item?.geometry)
                       }
                       self.tableView.reloadData()
            }

  }


}
