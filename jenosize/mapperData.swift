//
//  mapper.swift
//  jenosize
//
//  Created by Giftfy on 23/9/2563 BE.
//  Copyright © 2563 Giftfy. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation

struct RestaurantLocation: Mappable {
   var status: String = ""
   var results: [Result] = []
   init?(map: Map) {
   }
mutating func mapping(map: Map) {
   self.status <- map["status"]
   self.results <- map["results"]
   }
}
struct Result: Mappable {
   var geometry: Geometry?
   var icon: String = ""
   var name: String = ""
   var vicinity: String = ""
   init?(map: Map) {
   }
mutating func mapping(map: Map) {
   self.geometry <- map["geometry"]
   self.icon <- map["icon"]
   self.name <- map["name"]
    self.vicinity <- map["vicinity"]
   }
}
struct Geometry: Mappable {
   var lat: Double = 0.00
   var lng: Double = 0.00
   var location: CLLocation {
   return CLLocation(latitude: self.lat, longitude: self.lng)
}
   func distance(to location:CLLocation) -> Float {
      let distanceInMeters = location.distance(from: self.location)
      let distanceKm = Float(distanceInMeters/1000)
      return distanceKm
    }
   init?(map: Map) {
   }
mutating func mapping(map: Map) {
   self.lat <- map["location.lat"]
   self.lng <- map["location.lng"]
}
}
